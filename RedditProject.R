#This is a comaprably small code snippet I used for demonstrating the abi-
#lities of the RedditExtractoR package. It does some basic Reddit analysis.
#Create a new branch or anything if you think it needs to be fixed or ex-
#panded.
# Zoltán Schmidt, 08-Feb-2016
# (original code was last modified on 03-Dec-2015)

library("stringr")
library("RedditExtractor")

#-----------------------------------TEST-----------------------------------

#1:get_reddit - Get all data attributes from search query
test_getr <- get_reddit(search_terms = "Fallout 4")


#2:reddit_urls - Returns relevant reddit URLs
test_rurl <- reddit_urls(search_terms = "Fallout 4")


#3:construct_graph - Create a graph file from a single Reddit thread
construct_graph(test_getr[1:20,], plot = TRUE, write_to = NA)


#4:reddit_content -	Extract data attributes
test_rcon <- reddit_content(test_rurl[1,"URL"], wait_time = 2)


#-----------------------------------WORK-----------------------------------

#a) which are more popular: text posts, outer links or pictures?

#step zero, only for aiming purposes: a get_reddit sample
sample <- get_reddit(subreddit = subname,
                     cn_threshold = 20,
                     sort_by = "new")

#first, variables and use reddit_urls
upv_props <- c()
domainset <- c()
subname <- "gaming"
subname_alt <- paste("self",subname, sep=".")

data_fr <- reddit_urls(subreddit = subname,
                           page_threshold = 5,
                           sort_by = "new")


#then, gather every data about all the links
#and put only the upvote proportion and the domain to a dataframe
for (i in 1:100){
  temp <- reddit_content(data_fr[i,"URL"], wait_time = 2)
  
  #it skips if any of the necessary values are NA
  #so it' not the most efficient solution.
  
  if  (!is.na(temp[1,"upvote_prop"])) {
    upv_props <- c(upv_props,temp[1,"upvote_prop"])
  }
  
  if(!is.na(temp[1,"domain"])) {
    domainset <- c(domainset,temp[1,"domain"])    
  }
  
}

comparison_set <- data.frame(upv_props, domainset)

set_a <- comparison_set[domainset!=subname_alt,]
set_b <- comparison_set[domainset==subname_alt,]

#finally, calculate a mean for the two upvote prop sets
value_a = mean(as.numeric(as.character(set_a$upv_props)))
value_b = mean(as.numeric(as.character(set_b$upv_props)))

#b) calculate linear regression between various values
#in this example: comment length and comment score

#let's get a dataframe
subname <- "news"

data_fr <- get_reddit(search_terms = "",
                      subreddit = subname,
                      cn_threshold = 10,
                      page_threshold = 2,
                      wait_time = 2)

#the functions that do the work
looper <- function(var, min, max, cv){
 
  for (i in 1:length(data_fr$id)){
    
    if (var[i] > min && var[i] < max) {
      cv <- c(cv, var[i])
    }    
  }
  
  cv
  
}

df_maker <- function(ca, cb, result){
  
  l_a <- length(ca)
  l_b <- length(cb)
  
  if (l_a <= l_b){ result <- data.frame(a = ca,        b = cb[1:l_a])   
          } else { result <- data.frame(a = ca[1:l_b], b = cb)}
  
  result
  
}
  
#add the analyzed values to another dataframe
#length vs. score of comments
df_simpler <- c() 
words <- c() 
c_score <- c() 
lm_a <- c()

tempvar <- 0

min_l <- 5
max_l <- 1000
min_sc <- 2
max_sc <- 100
  

for (i in 1:length(data_fr$id)){
  
  #conditions for limiting length and score
  tempvar = str_count(data_fr$comment[i], pattern = " ")+1
  
  if (tempvar > min_l && tempvar < max_l) {
    words <- c(words, tempvar)
  } 

}

c_score = looper(data_fr$comment_score, min_sc, max_sc, c_score)

df_simpler <- df_maker(words, c_score, df_simpler)

#plot it out
lm_a <- lm(df_simpler$b ~ df_simpler$a)
plot(df_simpler$a, df_simpler$b, xlab = "comment length", ylab = "comment score")
abline(lm_a)
